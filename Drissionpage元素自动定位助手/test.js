//检查字符串是否为空
function isBlankString(str) {
    return str.trim().length === 0;
}
// 检查是否包含特殊字符 
function containsString(str) {
    return str.includes('href') || str.includes('src');
}

//打印某个元素的所有属性值
function printElementAttributesAsString(element) {
    // 检查输入是否是一个元素
    if (!(element instanceof Element)) {
        console.error('输入必须是一个HTML元素');
        return;
    }

    // 获取元素的所有属性
    var attrs = element.attributes;

    // 初始化一个空字符串用于存储属性
    var attributesString = '';

    // 遍历所有属性并将它们的名称和值拼接到字符串中
    for (var i = 0; i < attrs.length; i++) {
        var attrName = attrs[i].name;
        var attrValue = attrs[i].value;
        //特殊情况处理
        if (containsString(attrName)) continue;
        if (isBlankString(attrValue)) continue;

        if (attrValue.length > 25 && attrName != "class") {
            attributesString += "@@" + attrName + "^" + attrValue.slice(0, 20);
        } else {
            // 拼接属性名和属性值，属性之间用空格分隔
            attributesString += "@@" + attrName + "=" + attrValue;
        }



    }

    // 打印最终的属性字符串
    //console.log(attributesString.trim()); // 使用trim()移除尾部的空格
    return attributesString.trim();
}

//添加监听
function addClickEventToInputs() {
    // 获取所有输入框元素
    var inputElements = document.querySelectorAll('a,li,img,input,button');
    //var inputElements = document.querySelectorAll('*');

    // 为每个输入框元素添加点击事件监听器
    inputElements.forEach(function (inputElement) {
        inputElement.addEventListener('mouseover', function () {


            var attrib_info = printElementAttributesAsString(inputElement);

            var Name = "tag:" + inputElement.tagName.toLowerCase();

            var text = inputElement.innerText




            if (isBlankString(text)) {
                text = "";
            } else {

                if (text.length <= 15) text = "@@text()=" + text;
                else text = "@@text()^" + text.slice(0,10);

            }


            var info = "按F8复制元素语法--> " + Name + attrib_info + text


            document.getElementById('show').textContent = info;
        });
    });
}


// 调用函数，添加事件
addClickEventToInputs();